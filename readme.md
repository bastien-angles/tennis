# Jeu de Tennis

Cette petite application simule une partie de tennis avec ses jeux, sets et matchs. 

Elle prend en entr�e un fichier texte listant tous les �changes gagn�s au cours de la partie. Apr�s analyse de ce fichier, l'application fournit un rapport sur la progession du match dans la console.

## R�gles du Tennis

Vous pouvez consulter les r�gles du tennis � cette adresse.

[https://www.artengo.fr/conseils/les-regles-de-base-du-tennis-tp_56381.html](https://www.artengo.fr/conseils/les-regles-de-base-du-tennis-tp_56381.html).


## Installation

T�l�charger le d�p�t Git sur votre ordinateur.

```bash
git clone git@gitlab.com:bastien-angles/tennis.git
```

Lancer le projet dans votre IDE Java et ex�cuter la fonction `main` de la classe `Main.java`.


## Utilisation

L'application prend un fichier texte en entr�e. Ce fichier texte liste le gagnant de chaque �change effectu� au cours de la partie.
Ces donn�es peuvent �tre partielles : le match n'a pas besoin d'aboutir sur une victoire.
Vous pouvez trouver 2 exemples de fichiers de donn�es dans le dossier `/ressources` du projet.

2 types de progression peuvent �tre affich�es :
* Un �tat de la partie apr�s chaque �change
* Un �tat final de la partie une fois le match remport� ou le fichier texte enti�rement parcouru.

Exemple de retour
```bash
1 jeu partout, 30-15 pour Nadal
```

## Licence
[MIT](https://choosealicense.com/licenses/mit/)