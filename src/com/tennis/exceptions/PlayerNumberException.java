package com.tennis.exceptions;
public class PlayerNumberException extends Exception {

	public PlayerNumberException() {
		System.out.println("Vous ne pouvez jouer un match qu'avec 2 joueurs.");
	}
	
}
