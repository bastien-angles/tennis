package com.tennis.models;

public class Game extends TennisPart{
	
	private Player avantage;
	
	public Game(Player player1, Player player2) {
		super(player1, player2, 0, 0);
		this.setAvantage(null);
	}
	
	public void nextScore(Player player, int scorePlayer, int scoreOpponent) {
		int newScore = 0;
		switch(scorePlayer) {
			case 0:
				newScore = 15;
				break;
			case 15:
				newScore = 30;
				break;
			case 30:
				newScore = 40;
				break;
			case 40:
				// Si les deux joueurs sont � 40
				// Alors le joueur qui a marqu� prend l'avantage
				if(scoreOpponent == 40 && avantage == null) {
					newScore = 40;
					avantage = player;
					
				// Si le joueur qui a marqu� est � 40
			    // Et que le joueur adverse est aussi � 40 et a l'avantage
				// Alors les deux joueurs restent � 40 et personne n'a l'avantage
				} else if(scoreOpponent == 40 && avantage != player) {
					newScore = 40;
					avantage = null;
				}
				break;
		}
		
		if(newScore > 0 && player.equals(this.getPlayer1())) {
			this.setScorePlayer1(newScore);
			
		} else if(newScore > 0 && player.equals(this.getPlayer2())) {
			this.setScorePlayer2(newScore);
			
		}
	}
	
	@Override
	protected void incrementScore(Player player) {
		if(player.equals(this.getPlayer1())) {
			boolean winning = this.checkWinner(player, this.getScorePlayer1(), this.getScorePlayer2());
			if(winning) {
				this.setWinner(player);
			}
			
			// On affecte le nouveau score
			this.nextScore(player, this.getScorePlayer1(), this.getScorePlayer2());
			
		} else if(player.equals(this.getPlayer2())) {
			boolean winning = this.checkWinner(player, this.getScorePlayer2(), this.getScorePlayer1());
			if(winning) {
				this.setWinner(player);
			}
			
			// On affecte le nouveau score
			this.nextScore(player, this.getScorePlayer2(), this.getScorePlayer1());
		}
	}
	
	@Override
	protected boolean checkWinner(Player player, int scorePlayer, int scoreOpponent) {
		if ((scorePlayer == 40 && scoreOpponent < 40) || (scorePlayer == 40 && scoreOpponent == 40 && avantage == player)) {
			this.setWinner(player);
			return true;
		}
		return false;
	}
	
	@Override
	public void displayProgess(String playerName) {
		String notice = "Balle "+playerName+" // ";
		
		// D�tail du score
		notice += this.getPlayer1().getName()+" "+this.getScorePlayer1()+" - "+this.getPlayer2().getName()+" "+this.getScorePlayer2();
		
		if (this.avantage != null) {
			notice += " - avantage "+this.avantage.getName();
		} else if (this.getScorePlayer1() == 40 && this.getScorePlayer2() == 40) {
			notice += " - �galit�";
		}

		System.out.println(notice);
	
	}
	
	@Override
	public String tennisPartState() {
		String notice = this.getScorePlayer1()+"-"+this.getScorePlayer2();
		
		if (this.getScorePlayer1() != this.getScorePlayer2()) {
			notice += " pour ";
	
			if (this.getScorePlayer1() > this.getScorePlayer2()) {
				notice += this.getPlayer1().getName();
			} else {
				notice += this.getPlayer2().getName();
			}
		}

		// Exemple de r�sultat attendu : 30-40 pour Nadal
		return notice;
	}
	
	/**
	 *	GETTERS & SETTERS
	 */
	public Player getAvantage() {
		return avantage;
	}

	public void setAvantage(Player avantage) {
		this.avantage = avantage;
	}
	
}
