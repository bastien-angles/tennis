package com.tennis.models;

import java.util.ArrayList;
import java.util.List;

public class Match extends TennisPart{

	private List<Set> sets; 
	// On part du principe que le match est gagn� en 2 sets
	private final static int WINNING_SETS = 2;
	
	public Match(Player player1, Player player2) {
		super(player1, player2, 0, 0);
		this.sets = new ArrayList<Set>();
	}
	
	@Override
	public void incrementScore(Player player) {
		
		// On r�cup�re le Set en cours s'il existe
		Set lastSet = null;
		if (this.sets.size() > 0) {
			lastSet = this.sets.get(this.sets.size() - 1); 
		}
		
		// Si aucun Set n'existe
		// Ou si le Set en cours a un gagnant et donc termin�
		// On cr�e un nouveau Set
		if(lastSet == null || lastSet.getWinner() != null) {
			// D�termine le joueur ayant marqu� le point
			lastSet = new Set(this.getPlayer1(), this.getPlayer2());
			this.sets.add(lastSet);
		}

		// On appelle la fonction d'incr�mentation du Set, qui va appeler � son tour la fonction d'incr�ment du Jeu
		lastSet.incrementScore(player);
		
		// Si le Set en cours s'est termin� sur une victoire
		// Alors on modifie le score des Sets dans ce match
		if(lastSet.getWinner() != null) {
			
			if(this.getPlayer1().getName() == player.getName()) {
				
				// On ajoute un Set gagn� pour le joueur 1
				int scorePlayer1 = (Integer) this.getScorePlayer1() + 1;
				this.setScorePlayer1(scorePlayer1);
				// On v�rifie si le joueur 1 a gagn� le match
				this.checkWinner(player, this.getScorePlayer1(), this.getScorePlayer2());
				
			} else if(this.getPlayer2().getName() == player.getName()) {
				
				// On ajoute un Set gagn� pour le joueur 2
				int scorePlayer2 = (Integer) this.getScorePlayer2() + 1;
				this.setScorePlayer2(scorePlayer2);
				// On v�rifie si le joueur 2 a gagn� le match
				this.checkWinner(player, this.getScorePlayer2(), this.getScorePlayer1());
			}
		}
	}
	
	@Override
	protected boolean checkWinner(Player player, int scorePlayer, int scoreOpponent) {
		if (scorePlayer == WINNING_SETS) {
			this.setWinner(player);
			return true;
		}
		return false;
	}

	@Override
	// Score � afficher apr�s chaque �change
	// En param�tre le nom du gagnant de l'�change
	public void displayProgess(String playerName) {
		
		Set lastSet = this.sets.get(this.sets.size() - 1); 

		// Si le dernier Set � un gagnant alors on affiche le gagnant
		if(lastSet.getWinner() != null) {
			
			String notice = "Balle "+playerName;
			// On v�rifie si le dernier Set gagn� engendre la victoire du match
			if (this.getWinner() != null) {
				notice += " // Match gagn� par "+this.getWinner().getName();
				if (this.getWinner() == this.getPlayer1()) {
					notice += "("+this.getScorePlayer1()+" - "+this.getScorePlayer2()+")";
					
				} else {
					notice += "("+this.getScorePlayer2()+" - "+this.getScorePlayer1()+")";
				}
				
			} else {
				notice += " // Set "+lastSet.getWinner().getName();
	
				// D�tail du score
				notice += " ("+this.getPlayer1().getName()+" "+this.getScorePlayer1()+" - "+this.getPlayer2().getName()+" "+this.getScorePlayer2()+")";
			}

			System.out.println(notice);
			
		// Si le dernier Set est en cours alors on va afficher les d�tails de ce Set
		} else {
			lastSet.displayProgess(playerName);
		}
	}
	
	@Override
	public String tennisPartState() {
		String notice = "";
		if (this.getScorePlayer1() == this.getScorePlayer2()) {
			notice += this.getScorePlayer1();
			notice += (this.getScorePlayer1() > 1) ? " sets" : " set";
			notice += " partout, ";
			
		} else {
			notice += this.getScorePlayer1();
			notice += (this.getScorePlayer1() > 1) ? " sets " : " set ";
			notice += "pour "+this.getPlayer1().getName()+", ";
			notice += this.getScorePlayer2();
			notice += (this.getScorePlayer2() > 1) ? " sets " : " set ";
			notice += "pour "+this.getPlayer2().getName()+", ";
		}
		
		// Exemple de r�sultat attendu : 1 set pour Federer, 0 set pour Nadal
		return notice;
	}
	
	// R�capitulatif de la partie � un instant donn�
	public void displayFinal() {
		String notice = "";
		
		if (this.getWinner() != null) {
			notice += "Match remport� par "+this.getWinner().getName();
			if (this.getWinner() == this.getPlayer1()) {
				notice += "("+this.getScorePlayer1()+" - "+this.getScorePlayer2()+")";
				
			} else {
				notice += "("+this.getScorePlayer2()+" - "+this.getScorePlayer1()+")";
			}
			
		} else {
			if (this.getScorePlayer1() > 0 || this.getScorePlayer2() > 0) {
				notice = this.tennisPartState();
			}
			
			if (this.sets.size() > 0) {
				Set lastSet = this.sets.get(this.sets.size() - 1); 
				if (lastSet.getScorePlayer1() > 0 || lastSet.getScorePlayer2() > 0) {
					notice += lastSet.tennisPartState();
				}
			
				if(lastSet.getGames().size() > 0) {
					Game lastGame = lastSet.getGames().get(lastSet.getGames().size() - 1);
					notice += lastGame.tennisPartState();
				}
			}
		}
		
		System.out.println(notice);
	}
	
	
	/**
	 *	GETTERS & SETTERS
	 */
	public List<Set> getSets() {
		return sets;
	}

	public void setGames(ArrayList<Set> sets) {
		this.sets = sets;
	}

}
