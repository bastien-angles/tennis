package com.tennis.models;

import java.util.ArrayList;
import java.util.List;

public class Set extends TennisPart{

	private List<Game> games; 
	
	public Set(Player player1, Player player2) {
		super(player1, player2, 0, 0);
		this.games = new ArrayList<Game>();
		this.games.add(new Game(player1, player2));
	}
	
	@Override
	public void incrementScore(Player player) {
		// On r�cup�re le jeu en cours
		Game lastGame = null;
		if(games.size() > 0) {
			lastGame = games.get(games.size() - 1);
		}
		
		// Si aucun Jeu n'existe
		// Si le Jeu en cours a un gagnant et donc termin�
		// On cr�e un nouveau Jeu
		if(lastGame == null || lastGame.getWinner() != null) {
			lastGame = new Game(this.getPlayer1(), this.getPlayer2());
			games.add(lastGame);
		}
		
		// On appelle la fonction d'incr�ment du Jeu
		lastGame.incrementScore(player);
		
		// Si le Jeu en cours s'est termin� sur une victoire
		// Alors on modifie le score des Jeux dans ce Set
		if(lastGame.getWinner() != null) {
			
			if(this.getPlayer1().getName() == player.getName()) {
				
				// On ajoute un Jeu gagn� pour le joueur 1
				int scorePlayer1 = (Integer) this.getScorePlayer1() + 1;
				this.setScorePlayer1(scorePlayer1);
				// On v�rifie si le joueur 1 a gagn� le Set
				this.checkWinner(player, this.getScorePlayer1(), this.getScorePlayer2());
	
			} else if(this.getPlayer2().getName() == player.getName()) {
				
				// On ajoute un Jeu gagn� pour le joueur 2
				int scorePlayer2 = (Integer) this.getScorePlayer2() + 1;
				this.setScorePlayer2(scorePlayer2);
				// On v�rifie si le joueur 2 a gagn� le Set
				this.checkWinner(player, this.getScorePlayer1(), this.getScorePlayer2());
			}
		}
	}
	
	@Override
	protected boolean checkWinner(Player player, int scorePlayer, int scoreOpponent) {
		if (scorePlayer == 6 && scorePlayer - scoreOpponent >= 2) {
			this.setWinner(player);
			return true;
			
		// Si le joueur a 7 Jeux gagnants, alors son adversaire a forc�ment 5 ou 6 Jeux gagnants
		// C'est donc forc�ment une victoire
		} else if (scorePlayer == 7) {
			this.setWinner(player);
			return true;
		}
		
		return false;
	}
	
	@Override
	public void displayProgess(String playerName) {
		Game lastGame = games.get(games.size() - 1); 
		
		// Si le dernier Jeu � un gagnant alors on affiche "Jeu gagn�"
		if(lastGame.getWinner() != null) {
			String notice = "Balle "+playerName+" // Jeu "+lastGame.getWinner().getName();

			// D�tail du score
			notice += " ("+this.getPlayer1().getName()+" "+this.getScorePlayer1()+" - "+this.getPlayer2().getName()+" "+this.getScorePlayer2()+")";

			System.out.println(notice);
			
		// Si le dernier Jeu est en cours alors on va afficher les d�tails de ce Jeu
		} else {
			lastGame.displayProgess(playerName);
		}
	}
	
	@Override
	public String tennisPartState() {
		String notice = "";
		if (this.getScorePlayer1() == this.getScorePlayer2()) {
			notice += this.getScorePlayer1();
			notice += (this.getScorePlayer1() > 1) ? " jeux" : " jeu";
			notice += " partout, ";
			
		} else {
			notice += this.getScorePlayer1();
			notice += (this.getScorePlayer1() > 1) ? " jeux " : " jeu ";
			notice += "pour "+this.getPlayer1().getName()+", ";
			notice += this.getScorePlayer2();
			notice += (this.getScorePlayer2() > 1) ? " jeux " : " jeu ";
			notice += "pour "+this.getPlayer2().getName()+", ";
		}
		
		// Exemple de r�sultat attendu : 2 jeux pour Federer, 1 jeu pour Nadal
		return notice;
	}
	
	/**
	 *	GETTERS & SETTERS
	 */
	public List<Game> getGames() {
		return games;
	}

	public void setGames(ArrayList<Game> matches) {
		this.games = matches;
	}
	
}
