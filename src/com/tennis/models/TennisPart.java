package com.tennis.models;

public abstract class TennisPart {
	
	private Player player1;
	private Player player2;
	private Player winner;
	private int scorePlayer1;
	private int scorePlayer2;
	
	public TennisPart(Player player1, Player player2, int scorePlayer1, int scorePlayer2) {
		this.player1 = player1;
		this.player2 = player2;
		this.scorePlayer1 = scorePlayer1;
		this.scorePlayer2 = scorePlayer2;
	}
	
	protected abstract void incrementScore(Player player);
	
	protected abstract boolean checkWinner(Player player, int scorePlayer, int scoreOpponent);
	
	public abstract void displayProgess(String playerName);
	
	protected abstract String tennisPartState();
	
	/**
	 *	GETTERS & SETTERS
	 */
	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public int getScorePlayer1() {
		return scorePlayer1;
	}

	public void setScorePlayer1(int scorePlayer1) {
		this.scorePlayer1 = scorePlayer1;
	}

	public int getScorePlayer2() {
		return scorePlayer2;
	}

	public void setScorePlayer2(int scorePlayer2) {
		this.scorePlayer2 = scorePlayer2;
	}

	public Player getWinner() {
		return winner;
	}

	public void setWinner(Player winner) {
		this.winner = winner;
	}

}
