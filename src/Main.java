
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

import com.tennis.exceptions.EmptyFileException;
import com.tennis.exceptions.PlayerNumberException;
import com.tennis.models.Match;
import com.tennis.models.Player;

public class Main {

	public static void main(String[] args) throws PlayerNumberException, EmptyFileException {
		
		// 1 - On parcourt d'abord le document et on ins�re chaque gagnant dans un tableau
		// Dans ce cas, je pref�re tout r�unir dans un tableau pour r�aliser des v�rifications de base avant de lancer le traitement
		// L'acc�s � un ArrayList facilitera le processus, plut�t que de revenir plusieurs fois dans le fichier txt
		
		List<String> steps = new ArrayList<String>();
		try {

		    BufferedReader br = new BufferedReader(new FileReader("resources/match1.txt"));

		    String line;
		    while ((line = br.readLine()) != null) {
		        steps.add(line);
		    }

		    br.close();

		} catch (IOException ex) {
		    ex.printStackTrace();
		}
		
		// 2 - V�rification de base
		// * Est-ce que le fichier n'est pas vide ?
		// * Est-ce qu'on a bien 2 joueurs dans la partie ? (pas 1 ou + 2)
	    if(steps.size() == 0) {
	    	throw new EmptyFileException();
	    }
		
		List<String> players = steps.stream().distinct().collect(Collectors.toList());
	    if(players.size() != 2) {
	    	throw new PlayerNumberException();
	    }
	    
	    // 3 - Traitement
	    Player player1 = new Player(players.get(0));
	    Player player2 = new Player(players.get(1));
	    
	    Match match = new Match(player1, player2);
    	
	    for(int i = 0; i < steps.size(); i++) {

			if(player1.getName().equals(steps.get(i))) {
				match.incrementScore(player1);
	    		
	    	} else if(player2.getName().equals(steps.get(i))) {
	    		match.incrementScore(player2);
	    	}
			
			match.displayProgess(steps.get(i));
			
			// Si le match est gagn�, on cr�e un nouveau match
			if (match.getWinner() != null) {
				match = new Match(player1, player2);
				
				// Afficher le r�sultat � la fin du match
			    match.displayFinal();
			    System.out.println("");
			    
			}
	    }
	    
	    // Afficher le r�sultat final
	    match.displayFinal();
	}
}
